# Hayoo-Game-javascript
Hayoo game inspired by A Hangman game. Hangman is a paper and pencil guessing game for tries to guess it by suggesting letters of guesses.This game includes some basic programming concepts such as storing values in variables, working with arrays, using conditional statements, and using different types of loops.

[Live Demo](https://example-javascript.herokuapp.com/hayoo)
